"""
    Demo project for logging
"""

import logging
import logging.config
import os
import yaml


def main():
    """
    Demo project for logging
    """
    # Logger configuration
    with open("conf_logging.yaml", mode="rt") as conf_file:
        logging.config.dictConfig(yaml.safe_load(conf_file.read()))

    # Create logger and override LOGLEVEL with env var
    logger = logging.getLogger("kube-logger")
    logger.setLevel(os.environ.get("LOGLEVEL", "INFO"))

    # Add optional context in each line of log
    if os.environ.get("POD_NAME"):
        logger = logging.LoggerAdapter(
            logger,
            {
                "kubernetes": {
                    "namespace_name": os.environ.get("POD_NAMESPACE"),
                    "pod_name": os.environ.get("POD_NAME"),
                }
            },
        )

    # Examples
    try:
        logger.info(f"bla bla info de niveau {logger.getEffectiveLevel()}")
        logger.error("bla bla error", extra={"id_correlation": "666"})
    except:
        logger.error("ops...")
        raise

    # Add context on a try-except (will produce a level ERROR)
    try:
        1 / "et non"
    except:
        logger.exception("erf")


if __name__ == "__main__":
    main()
