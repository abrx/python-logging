# Python logging

Le but de ce repo est de fournir un exemple de configuration pour le [logging](https://docs.python.org/3/library/logging.html) python.

## Rappel du fonctionnement:

* On a un objet logger partageable entre plusieurs scripts / fonctions (NB: il faut redéclarer le logger dans les fonctions)
* On lui assigne un formatter qui va structurer l'output
* On lui assigne un handler qui spécifie où logger
* A chaque niveau on filtre via le **level** : CRITICAL, ERROR, WARN, INFO, DEBUG... (ou une valeur littérale)
* on remplace les print par un appel au logger


## Dans l'exemple :

* configuration externe en yaml
* formattage json possible via [plugin](https://github.com/madzak/python-json-logger)
* ajout de champs supplémentaires
* ajout de champs par défaut
* logs du contexte des stacktrace lors d'un except

> Par défaut là on log en json en Stream (stdout/stderr), typiquement pour un environnement kube, mais il y a aussi des exemples de formatter standards avec un file handler.

## Utilisation

Setup de l'environnement (plus le clone et mkdir...):

```bash
python -m venv ../venv/python-logging
source ../venv/python-logging/bin/activate
pip install -r requirements.txt
```

Tests:

```bash
$ python main.py 
{"message": "bla bla info de niveau 20", "timestamp": "2023-07-25T10:27:54Z", "severity": "INFO"}
{"message": "bla bla error", "id_correlation": "666", "timestamp": "2023-07-25T10:27:54Z", "severity": "ERROR"}
{"message": "erf", "exc_info": "Traceback (most recent call last):\n  File \"main.py\", line 45, in main\n    1 / \"et non\"\nTypeError: unsupported operand type(s) for /: 'int' and 'str'", "timestamp": "2023-07-25T10:27:54Z", "severity": "ERROR"}
```

Avec des infos ajoutées à chaque ligne:

```bash
$ export POD_NAME=pod-666
$ export POD_NAMESPACE=default
$ python main.py 
{"message": "bla bla info de niveau 20", "kubernetes": {"namespace_name": "default", "pod_name": "pod-666"}, "timestamp": "2023-07-25T10:28:38Z", "severity": "INFO"}
{"message": "bla bla error", "kubernetes": {"namespace_name": "default", "pod_name": "pod-666"}, "timestamp": "2023-07-25T10:28:38Z", "severity": "ERROR"}
{"message": "erf", "exc_info": "Traceback (most recent call last):\n  File \"main.py\", line 45, in main\n    1 / \"et non\"\nTypeError: unsupported operand type(s) for /: 'int' and 'str'", "kubernetes": {"namespace_name": "default", "pod_name": "pod-666"}, "timestamp": "2023-07-25T10:28:38Z", "severity": "ERROR"}
```

Avec LOGLEVEL différent (les INFO ont disparu, on ne prend que les level supérieurs ou égaux à ERROR):

```bash
$ export LOGLEVEL=ERROR
$ python main.py 
{"message": "bla bla error", "kubernetes": {"namespace_name": "default", "pod_name": "pod-666"}, "timestamp": "2023-07-25T10:40:43Z", "severity": "ERROR"}
{"message": "erf", "exc_info": "Traceback (most recent call last):\n  File \"main.py\", line 45, in main\n    1 / \"et non\"\nTypeError: unsupported operand type(s) for /: 'int' and 'str'", "kubernetes": {"namespace_name": "default", "pod_name": "pod-666"}, "timestamp": "2023-07-25T10:40:43Z", "severity": "ERROR"}
```

> Rappel des level: CRITICAL=50, ERROR=40, WARNING=30, INFO=20, DEBUG=10

Tests de logs dans des fichiers avec le `classic-logger`:

```bash
$ export LOGLEVEL=INFO
$ sed -i 's/"kube-logger"/"classic-logger"/' main.py

$ python main.py

# que les erreurs
$ cat /tmp/errors.log 
ERROR <PID 18055:MainProcess> classic-loggermain(): bla bla error
ERROR <PID 18055:MainProcess> classic-loggermain(): erf
Traceback (most recent call last):
  File "main.py", line 45, in main
    1 / "et non"
TypeError: unsupported operand type(s) for /: 'int' and 'str'

# tous les logs avec un loglevel supérieur à celui set
$ cat /tmp/test.log 
2023-07-25 10:42:48,285 - classic-logger - INFO - bla bla info de niveau 20
2023-07-25 10:42:48,285 - classic-logger - ERROR - bla bla error
2023-07-25 10:42:48,285 - classic-logger - ERROR - erf
Traceback (most recent call last):
  File "main.py", line 45, in main
    1 / "et non"
TypeError: unsupported operand type(s) for /: 'int' and 'str'
```